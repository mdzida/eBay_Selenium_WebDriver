# eBay - Selenium Webdriver Tests
### Description
The aim of Selenium Webdriver tests was commonly known online store - eBay. Main focus was put on filter options, through which finding products is much more easier. Beside that, login and logout tests was also carried.

Project is created with use of Page Object Model pattern. 
Technologies used: Java, Selenium Webdriver, TestNG, AssertJ.

### Requirements
In order to run tests locally one need to have installed: 

- Java Development Kit (JDK)

### How to run
The best way to run tests is by using Maven with included to the project pom.xml file. Then all what is needed, is just simply click the -run- arrow next to chosen test. 

