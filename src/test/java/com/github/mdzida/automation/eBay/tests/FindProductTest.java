package com.github.mdzida.automation.eBay.tests;

import com.github.mdzida.automation.eBay.components.FillInputs;
import com.github.mdzida.automation.eBay.config.Config;
import com.github.mdzida.automation.eBay.pages.ItemsPage;
import com.github.mdzida.automation.eBay.pages.LandingPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import static org.assertj.core.api.Assertions.assertThat;

public class FindProductTest {
    private static final String JEWELERY = "Biżuteria";
    private static final String CAR = "Samochód";
    private static final String MOTO = "Motoryzacja";
    private static final String TOYS = "Zabawki";

    private WebDriver driver;
    private LandingPage landingPage;
    private ItemsPage itemsPage;
    private FillInputs fillInputs;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();
        landingPage = new LandingPage(driver);
        itemsPage = new ItemsPage(driver);
        fillInputs = new FillInputs();
    }

    @BeforeMethod
    public void openLandingPage() {
        landingPage.openHomePage();
    }

    @Test
    public void searchForJewellery() {
        fillInputs.fillInputField(landingPage.getSearchField(), JEWELERY);
        fillInputs.submitInputField(landingPage.getSearchField());
        assertThat(itemsPage.getNameOfSearchedField()).isEqualTo(JEWELERY);
        assertThat(itemsPage.getNameOfSearchedSubField()).startsWith(JEWELERY);
    }

    @Test
    public void searchForCars() {
        fillInputs.fillInputField(landingPage.getSearchField(), CAR);
        fillInputs.submitInputField(landingPage.getSearchField());
        assertThat(itemsPage.getNameOfSearchedField()).isEqualTo(CAR);
        assertThat(itemsPage.getNameOfSearchedSubField()).startsWith(MOTO);
    }

    @Test
    public void searchForToys() {
        fillInputs.fillInputField(landingPage.getSearchField(), TOYS);
        fillInputs.submitInputField(landingPage.getSearchField());
        assertThat(itemsPage.getNameOfSearchedField()).isEqualTo(TOYS);
        assertThat(itemsPage.getNameOfSearchedSubField()).isEqualTo(TOYS);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}