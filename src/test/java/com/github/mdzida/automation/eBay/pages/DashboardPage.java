package com.github.mdzida.automation.eBay.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class DashboardPage {
    private static final By NAME_ACCOUNT_BUTTON = By.xpath("//*[@id='gh-ug']/b[1]");
    private static final By ACCOUNT_BUTTON = By.id("gh-ug");
    private static final By LOGOUT = By.linkText("Wyloguj się");

    private WebDriver driver;

    public DashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getAccountName() {
        return driver.findElement(NAME_ACCOUNT_BUTTON).getText();
    }

    public void logOut() {
        goToAccountDetails();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.findElement(LOGOUT).click();
    }

    private void goToAccountDetails() {
        Actions builder = new Actions(driver);
        builder.moveToElement(driver.findElement(ACCOUNT_BUTTON)).perform();
        driver.findElement(ACCOUNT_BUTTON).click();
    }
}
