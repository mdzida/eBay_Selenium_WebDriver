package com.github.mdzida.automation.eBay.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
    private static final String EBAY_HOME_PAGE_URL = "https://www.ebay.pl";
    private static final By SELL_FIELD = By.linkText("Sprzedaj");
    private static final By SEARCH_BUTTON = By.id("gh-btn");
    private static final By LOGIN_BUTTON = By.linkText("Zaloguj się");
    private static final By SEARCH_CATEGORIES_BUTTON = By.id("gh-shop-a");
    private static final By CAT_HOME_AND_FURNITURE = By.partialLinkText("Dom");
    private static final By CAT_COMPANY = By.partialLinkText("Firma");
    private static final By CAT_WOMAN_CLOTHES = By.partialLinkText("damska");
    private static final By CAT_MOBILES_ACCESSORIES = By.partialLinkText("Telefony");
    private static final By SEARCH_FIELD = By.id("gh-ac");

    private WebDriver driver;

    public LandingPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getSearchField(){
        return driver.findElement(SEARCH_FIELD);
    }

    public void selectHomeAndFurnitureCategory() {
        driver.findElement(CAT_HOME_AND_FURNITURE).click();
    }

    public void selectCompanyCategory(){
        driver.findElement(CAT_COMPANY).click();
    }

    public void selectWomanClothes(){
        driver.findElement(CAT_WOMAN_CLOTHES).click();
    }

    public void selectMobilesAndAccessories(){
        driver.findElement(CAT_MOBILES_ACCESSORIES).click();
    }

    public String getSellText() {
        return driver.findElement(SELL_FIELD).getText();
    }

    public WebElement getSearchButton() {
        return driver.findElement(SEARCH_BUTTON);
    }

    public WebElement getLoginButton() {
        return driver.findElement(LOGIN_BUTTON);
    }

    public void openPageThenNavigateToLoginPage() {
        openHomePage();
        navigateToLoginPage();
    }

    private void navigateToLoginPage() {
        driver.findElement(LOGIN_BUTTON).click();
    }

    public void openPageAndSearch(){
        openHomePage();
        clickOnSearchCategoriesButton();
    }

    public void clickOnSearchCategoriesButton() {
        driver.findElement(SEARCH_CATEGORIES_BUTTON).click();
    }

    public LandingPage openHomePage() {
        driver.get(EBAY_HOME_PAGE_URL);
        return this;
    }
}