package com.github.mdzida.automation.eBay.tests;

import com.github.mdzida.automation.eBay.config.Config;
import com.github.mdzida.automation.eBay.pages.ItemsPage;
import com.github.mdzida.automation.eBay.pages.LandingPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import static org.assertj.core.api.Assertions.assertThat;

public class NavigateThroughCategoriesTest {
    private static final String HOME_AND_FURNITURE = "Dom i Meble";
    private static final String HOME = "Dom";
    private static final String COMPANY = "Firma";
    private static final String WOMAN_CLOTHES = "Odzież damska";
    private static final String FOR_WOMAN = "damska";
    private static final String MOBILES_AND_ACESSORIES = "Telefony i Akcesoria";
    private static final String MOBILES = "Telefony";

    private WebDriver driver;
    private LandingPage landingPage;
    private ItemsPage itemsPage;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();
        landingPage = new LandingPage(driver);
        itemsPage = new ItemsPage(driver);
    }

    @BeforeMethod
    public void openPageAndSearch(){
        landingPage.openPageAndSearch();
    }

    @Test
    public void checkSelectionHomeAndFurniture() {
        landingPage.selectHomeAndFurnitureCategory();
        assertThat(itemsPage.getSearchedPane()).isEqualTo(HOME_AND_FURNITURE);
        assertThat(itemsPage.checkRightSelection()).contains(HOME);
    }

    @Test
    public void checkSelectionCollections() {
        landingPage.selectCompanyCategory();
        assertThat(itemsPage.getSearchedPane()).contains(COMPANY);
        assertThat(itemsPage.checkRightSelection()).contains(COMPANY);
    }

    @Test
    public void checkSelectionWomanClothes() {
        landingPage.selectWomanClothes();
        assertThat(itemsPage.getSearchedPane()).isEqualTo(WOMAN_CLOTHES);
        assertThat(itemsPage.checkRightSelection()).contains(FOR_WOMAN);
    }

    @Test
    public void checkSelectionMobilesAndAccessories() {
        landingPage.selectMobilesAndAccessories();
        assertThat(itemsPage.getSearchedPane()).isEqualTo(MOBILES_AND_ACESSORIES);
        assertThat(itemsPage.checkRightSelection()).contains(MOBILES);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}