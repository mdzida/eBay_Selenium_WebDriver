package com.github.mdzida.automation.eBay.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
    private static final By USERNAME = By.id("userid");
    private static final By PASSWORD = By.id("pass");
    private static final By SIGN_IN_BUTTON = By.id("sgnBt");
    private static final By LOGIN_ERROR = By.id("errf");

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isSignInButtonDisplayed() {
        return driver.findElement(SIGN_IN_BUTTON).isDisplayed();
    }

    public void logIn(String inputUsername, String inputPassword) {
        driver.findElement(USERNAME).clear();
        driver.findElement(USERNAME).sendKeys(inputUsername);
        driver.findElement(PASSWORD).clear();
        driver.findElement(PASSWORD).sendKeys(inputPassword);
        driver.findElement(SIGN_IN_BUTTON).click();
    }

    public WebElement getErrorLogin() {
        return driver.findElement(LOGIN_ERROR);
    }
}