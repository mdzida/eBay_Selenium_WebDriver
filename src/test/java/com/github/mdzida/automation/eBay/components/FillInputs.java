package com.github.mdzida.automation.eBay.components;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class FillInputs {
    public void fillInputField(WebElement input, String characters) {
        input.sendKeys(characters);
    }

    public void submitInputField(WebElement input) {
        input.sendKeys(Keys.ENTER);
    }
}
