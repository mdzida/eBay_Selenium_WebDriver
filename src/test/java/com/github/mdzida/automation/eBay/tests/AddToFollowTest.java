package com.github.mdzida.automation.eBay.tests;

import com.github.mdzida.automation.eBay.config.Config;
import com.github.mdzida.automation.eBay.pages.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import static org.assertj.core.api.Assertions.assertThat;

public class AddToFollowTest {
    private static final String ADD_TO = "Dodaj do";
    private static final String ADDED = "Dodałeś";
    private static final String USERNAME = Config.config().getProperty("username");
    private static final String PASSWORD = Config.config().getProperty("password");

    private WebDriver driver;
    private LandingPage landingPage;
    private LoginPage loginPage;
    private ItemsPage itemsPage;
    private ItemDetailsPage itemDetailsPage;
    private DashboardPage dashboardPage;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();
        landingPage = new LandingPage(driver);
        loginPage = new LoginPage(driver);
        itemsPage = new ItemsPage(driver);
        itemDetailsPage = new ItemDetailsPage(driver);
        dashboardPage = new DashboardPage(driver);
    }

    @BeforeMethod
    public void goToLocation() {
        landingPage.openPageThenNavigateToLoginPage();
        loginPage.logIn(USERNAME, PASSWORD);
        landingPage.clickOnSearchCategoriesButton();
        landingPage.selectCompanyCategory();
    }

    @AfterMethod
    public void logout() {
        dashboardPage.logOut();
    }

    @Test
    public void checkIfAddProductPageIsDisplayed() {
        itemsPage.getFirstItem();
        assertThat(itemDetailsPage.getFollow()).contains(ADD_TO);
    }

    @Test
    public void addItemToFollowed() {
        itemsPage.getFirstItem();
        itemDetailsPage.addToFollowed();
        assertThat(itemDetailsPage.getAddedToFollow().contains(ADDED));
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
