package com.github.mdzida.automation.eBay.tests;

import com.github.mdzida.automation.eBay.config.Config;
import com.github.mdzida.automation.eBay.pages.LandingPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class LandingPageTest {
    private static final String SELL = "Sprzedaj";

    private WebDriver driver;
    private LandingPage landingPage;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();
        landingPage = new LandingPage(driver);
    }

    @Test
    public void testSuccessfullyAccessLandingPage() {
        landingPage.openHomePage();
        assertThat(landingPage.getSellText()).isEqualTo(SELL);
        assertThat(landingPage.getSearchButton().isDisplayed()).isTrue();
        assertThat(landingPage.getLoginButton().isDisplayed()).isTrue();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
