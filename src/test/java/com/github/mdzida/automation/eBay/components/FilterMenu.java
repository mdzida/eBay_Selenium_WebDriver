package com.github.mdzida.automation.eBay.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.xml.bind.annotation.XmlType;
import java.util.List;

public class FilterMenu {
    private static final int DEFAULT_NUMBER_OF_FILTERED_ELEMENTS = 5;
    private WebDriver driver;

    public FilterMenu(WebDriver driver){
        this.driver = driver;
    }

    public void selectByFilter(By filter){
        driver.findElement(filter).click();
    }

    public String[] getArrayOfFilteredItemsByAttribute(By filterThrough, String attributeName){
        List<WebElement> allElements = driver.findElements(filterThrough);
        String[] filteredArray = new String[DEFAULT_NUMBER_OF_FILTERED_ELEMENTS];
        for (int i = 0; i < filteredArray.length; i++) {
            filteredArray[i] = allElements.get(i).getAttribute(attributeName);
        }
        return filteredArray;
    }

    public String[] getArrayOfFilteredItemsByClass(By filterThrough){
        List<WebElement> allElements = driver.findElements(filterThrough);

        String[] filteredArray = new String[DEFAULT_NUMBER_OF_FILTERED_ELEMENTS];
        for (int i = 0; i < filteredArray.length; i++) {
            filteredArray[i] = allElements.get(i).getText();
        }
        return filteredArray;
    }
}
