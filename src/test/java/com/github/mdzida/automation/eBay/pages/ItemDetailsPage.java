package com.github.mdzida.automation.eBay.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ItemDetailsPage {
    private static final By FOLLOW = By.className("vi-atw-txt");
    private static final By ADDED_TO_FOLLOW = By.id("w1-5-_lmsg");

    private WebDriver driver;

    public ItemDetailsPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getAddedToFollow(){
        return driver.findElement(ADDED_TO_FOLLOW).getTagName();
    }

    public void addToFollowed(){
        driver.findElement(FOLLOW).click();
    }

    public String getFollow() {
        return driver.findElement(FOLLOW).getText();
    }
}
