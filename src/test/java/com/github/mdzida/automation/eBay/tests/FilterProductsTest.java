package com.github.mdzida.automation.eBay.tests;

import com.github.mdzida.automation.eBay.components.FillInputs;
import com.github.mdzida.automation.eBay.config.Config;
import com.github.mdzida.automation.eBay.pages.ItemsPage;
import com.github.mdzida.automation.eBay.pages.LandingPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterProductsTest {
    private static final String BUY = "Kup";
    private static final String USED = "Używany";
    private static final String NEW = "Fabrycznie nowy";
    private static final String RENEW = "Odnowiony";
    private static final String PARTS = "na części";
    private static final String PRICE_FROM = "50";
    private static final String PRICE_TO = "20";

    private WebDriver driver;
    private LandingPage landingPage;
    private ItemsPage itemsPage;
    private FillInputs fillInputs;

    @BeforeClass()
    public void setUp() {
        driver = Config.getDefaultDriver();
        landingPage = new LandingPage(driver);
        itemsPage = new ItemsPage(driver);
        fillInputs = new FillInputs();
    }

    @BeforeMethod
    public void openAndSelectCorrectCategory(){
        landingPage.openPageAndSearch();
        landingPage.selectHomeAndFurnitureCategory();
    }

    @Test
    public void filterByBuyNow() {
        String[] buyNowItems = itemsPage.displayAndGetOnlyBuyNowItems();
        assertThat(buyNowItems).isNotEmpty();

        for (String item : buyNowItems) {
            assertThat(item).contains(BUY);
        }
    }

    @Test
    public void filerBySecondHand() {
        String[] secondHandItems = itemsPage.displayAndGetOnlySecondHandItems();
        assertThat(secondHandItems).isNotEmpty();

        for (String item : secondHandItems) {
            assertThat(item).isEqualTo(USED);
        }
    }

    @Test
    public void filterByNew() {
        String[] newItems = itemsPage.displayAndGetOnlyNewItems();
        assertThat(newItems).isNotEmpty();

        for (String item : newItems) {
            assertThat(item).isEqualTo(NEW);
        }
    }

    @Test
    public void filterByRenewedBySeller() {
        String[] renewedItems = itemsPage.displayAndGetOnlyRenewedBySellerItems();
        assertThat(renewedItems).isNotEmpty();

        for (String item : renewedItems) {
            assertThat(item).isEqualTo(RENEW);
        }
    }

    @Test
    public void filterBySpoiled() {
        String[] spoiledItems = itemsPage.displayAndGetSpoiledItems();
        assertThat(spoiledItems).isNotEmpty();

        for (String item : spoiledItems) {
            assertThat(item).contains(PARTS);
        }
    }

    @Test //encountered a - prices aren't properly filtered
    public void filterByPrice(){
        fillInputs.fillInputField(itemsPage.getPriceInputFrom(), PRICE_FROM);
        fillInputs.fillInputField(itemsPage.getPriceInputTo(), PRICE_TO);
        itemsPage.submitPrices();

        String[] itemsFilteredByPrice = itemsPage.getItemsFilteredByPrice();

        ArrayList<Double> filteredPrices = new ArrayList<Double>();
        for (String item : itemsFilteredByPrice) {
            Double price = mapToNumber(item);
            filteredPrices.add(price);
        }

        assertThat(filteredPrices).isNotEmpty();

        for (Double item : filteredPrices) {
            assertThat(item).isBetween(20.0, 50.0);
        }
    }

    private Double mapToNumber(String itemPrice) {
        return Double.parseDouble(itemPrice.substring(0, 4).replace(",", "."));
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
