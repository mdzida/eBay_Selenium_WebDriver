package com.github.mdzida.automation.eBay.tests;

import com.github.mdzida.automation.eBay.config.Config;
import com.github.mdzida.automation.eBay.pages.DashboardPage;
import com.github.mdzida.automation.eBay.pages.LandingPage;
import com.github.mdzida.automation.eBay.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class LoginLogoutTest {
    private static final String USERNAME = Config.config().getProperty("username");
    private static final String PASSWORD = Config.config().getProperty("password");
    private static final String FAKE_USERNAME = Config.config().getProperty("fakeUsername");
    private static final String FAKE_PASSWORD = Config.config().getProperty("fakePassword");
    private static final String LOGIN = Config.config().getProperty("login");

    private WebDriver driver;
    private LandingPage landingPage;
    private LoginPage loginPage;
    private DashboardPage dashboardPage;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();
        loginPage = new LoginPage(driver);
        dashboardPage = new DashboardPage(driver);
        landingPage = new LandingPage(driver);
    }

    private void login() {
        landingPage.openPageThenNavigateToLoginPage();
        assertThat(loginPage.isSignInButtonDisplayed()).isTrue();

        loginPage.logIn(USERNAME, PASSWORD);
        assertThat(dashboardPage.getAccountName()).contains(LOGIN);
    }

    private void logout() {
        dashboardPage.logOut();
        assertThat(landingPage.getLoginButton().isDisplayed()).isTrue();
    }

    private void loginWithFakeData() {
        landingPage.openPageThenNavigateToLoginPage();

        loginPage.logIn(FAKE_USERNAME, PASSWORD);
        assertThat(loginPage.getErrorLogin().isDisplayed()).isTrue();

        loginPage.logIn(USERNAME, FAKE_PASSWORD);
        assertThat(loginPage.getErrorLogin().isDisplayed()).isTrue();
    }

    @Test
    public void loginAndLogout() {
        login();
        logout();
        loginWithFakeData();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
