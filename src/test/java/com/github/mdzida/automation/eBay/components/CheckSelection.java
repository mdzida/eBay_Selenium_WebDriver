package com.github.mdzida.automation.eBay.components;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CheckSelection {

    public String checkWhichOptionIsSelected(WebElement dropdownPath) {
        Select select = new Select(dropdownPath);
        WebElement option = select.getFirstSelectedOption();
        return option.getText();
    }
}
