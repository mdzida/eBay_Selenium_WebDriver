package com.github.mdzida.automation.eBay.pages;

import com.github.mdzida.automation.eBay.components.CheckSelection;
import com.github.mdzida.automation.eBay.components.FilterMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ItemsPage {
    private static final By SELECTED_FIELD = By.className("cat-app");
    private static final By BUY_NOW_RBTN = By.xpath("//*[@title=\"Kup teraz\"]");
    private static final By BUY_NOW_FIELD = By.className("logoBin");
    private static final By SECOND_HAND_CKBX = By.id("e1-36");
    private static final By NEW_CKBX = By.xpath("//*[@id=\"e1-27\"]/a");
    private static final By RENEWED_BY_SELLER_CKBX = By.xpath("//*[@id=\"e1-33\"]/a");
    private static final By SPOILED_CKBX = By.xpath("//*[@id=\"e1-37\"]/a");
    private static final By ABOUT_STATE = By.className("lvsubtitle");
    private static final By DROPDOWN_SELECTION = By.id("gh-cat");
    private static final By PRODUCT_FIELD = By.xpath("//*[@id=\"cbelm\"]/div[3]/h1/span[2]/b");
    private static final By PRODUCT_SUBFIELD = By.xpath("//*[@id=\"e1-12\"]/div[2]/div[2]/div[1]/a");
    private static final By PRICE_INPUT_FROM = By.name("_udlo");
    private static final By PRICE_INPUT_TO = By.name("_udhi");
    private static final By ABOUT_PRICE = By.className("prRange");
    private static final By SUBMIT_PRICE = By.xpath("//*[@id=\"e1-41\"]");
    private static final By FIRST_ITEM = By.cssSelector("img[class=\"img\"]");
    private static final String TITLE = "title";

    private WebDriver driver;
    private FilterMenu filterMenu;
    private CheckSelection checkSelection;

    public ItemsPage(WebDriver driver) {
        this.driver = driver;
        filterMenu = new FilterMenu(driver);
        checkSelection = new CheckSelection();
    }

    public void getFirstItem(){
        driver.findElements(FIRST_ITEM).get(0).click();
    }

    public String[] getItemsFilteredByPrice() {
        return filterMenu.getArrayOfFilteredItemsByClass(ABOUT_PRICE);
    }

    public void submitPrices() {
        driver.findElement(SUBMIT_PRICE).click();
    }

    public WebElement getPriceInputTo() {
        return driver.findElement(PRICE_INPUT_TO);
    }

    public WebElement getPriceInputFrom() {
        return driver.findElement(PRICE_INPUT_FROM);
    }

    public String getNameOfSearchedSubField() {
        return driver.findElement(PRODUCT_SUBFIELD).getText();
    }

    public String getNameOfSearchedField() {
        return driver.findElement(PRODUCT_FIELD).getText();
    }

    public String getSearchedPane() {
        return driver.findElement(SELECTED_FIELD).getText();
    }

    public String[] displayAndGetOnlyBuyNowItems() {
        filterMenu.selectByFilter(BUY_NOW_RBTN);
        return filterMenu.getArrayOfFilteredItemsByAttribute(BUY_NOW_FIELD, TITLE);
    }

    public String[] displayAndGetOnlySecondHandItems() {
        filterMenu.selectByFilter(SECOND_HAND_CKBX);
        return filterMenu.getArrayOfFilteredItemsByClass(ABOUT_STATE);
    }

    public String[] displayAndGetOnlyNewItems() {
        filterMenu.selectByFilter(NEW_CKBX);
        return filterMenu.getArrayOfFilteredItemsByClass(ABOUT_STATE);
    }

    public String[] displayAndGetOnlyRenewedBySellerItems() {
        filterMenu.selectByFilter(RENEWED_BY_SELLER_CKBX);
        return filterMenu.getArrayOfFilteredItemsByClass(ABOUT_STATE);
    }

    public String[] displayAndGetSpoiledItems() {
        filterMenu.selectByFilter(SPOILED_CKBX);
        return filterMenu.getArrayOfFilteredItemsByClass(ABOUT_STATE);
    }

    public String checkRightSelection() {
        return checkSelection.checkWhichOptionIsSelected(driver.findElement(DROPDOWN_SELECTION));
    }
}